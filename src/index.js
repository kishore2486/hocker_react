import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';
import Cookies from 'js-cookie';
import App from './App';
import * as serviceWorker from './serviceWorker';
import rootReducer from './store/reducers';
import { userAction } from './store/reducers/login';

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

if (Cookies.get('user')) {
  const userinfo = JSON.parse(Cookies.get('user'));
  store.dispatch(userAction(userinfo));
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

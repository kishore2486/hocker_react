/* eslint-disable no-nested-ternary */
/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Input from '../Form/input';
import { userLogin } from '../../store/reducers/login';

class Login extends React.PureComponent {
  state = {
    email: '',
    emailError: false,
    password: '',
    passwordError: false,
  };

  // shouldComponentUpdate(nextProps, nextState) {
  //   const { emailError, passwordError } = this.state;
  //   return (
  //     nextState.emailError !== emailError ||
  //     nextState.passwordError !== passwordError
  //   );
  // }
  componentWillReceiveProps(nextProps) {
    const { isLoggedIn, loaded, history } = nextProps;
    if (isLoggedIn && loaded) {
      this.setState({
        email: '',
        password: '',
      });
      setTimeout(() => history.push('/user/dashboard'), 200);
    }
  }

  handleEmail = e => {
    return /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim.test(e.target.value)
      ? this.setState({
        emailError: false,
        email: e.target.value,
      })
      : this.setState({
        emailError: true,
      });
  };

  handlePassword = e => {
    return e.target.value.length >= 8 && e.target.value.length <= 16
      ? this.setState({
        passwordError: false,
        password: e.target.value,
      })
      : this.setState({
        passwordError: true,
      });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { email, emailError, password, passwordError } = this.state;
    const { userLogin } = this.props;
    if (!(emailError && passwordError)) {
      userLogin({ email, password });
    }
  };

  render() {
    const { email, emailError, password, passwordError } = this.state;
    const { loading, loaded, isLoggedIn, error } = this.props;
    console.log(error);
    const h6Style = {
      float: 'right',
      lineHeight: '2.7rem',
      fontSize: '14px',
      cursor: 'pointer',
    };
    return (
      <form onSubmit={this.handleSubmit}>
        <Input
          label="Email"
          htmlFor="email"
          type="email"
          placeholder="Enter your email"
          value={email}
          change={this.handleEmail}
          error={emailError ? 'Enter your valid email address' : ''}
        />
        <Input
          label="Password"
          htmlFor="password"
          type="password"
          placeholder="Enter your password"
          value={password}
          change={this.handlePassword}
          error={passwordError ? 'Password must be 8 to 10 characters' : ''}
        />
        <button type="submit" className="btn btn-success">
          {loading ? 'loading..' : loaded && isLoggedIn ? 'Redirecting..' : 'LogIn'}
        </button>
        <h6 style={h6Style}>Forgot password?</h6>
        {error && (
          <div className={`alert ${'alert-danger'}`} style={{ marginTop: '15px' }} role="alert">
            <span>{error.msg}</span>
          </div>
        )}
      </form>
    );
  }
}

const mapStateToProps = ({ login, user }) => ({
  ...login,
  ...user,
});

const login = withRouter(Login);

export default connect(
  mapStateToProps,
  { userLogin }
)(login);

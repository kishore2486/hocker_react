import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { StyledModal, Backdrop } from '../Styled/utils';
import style from './index.module.css';

const Modal = ({ children, show, clicked }) => {
  if (!show) return null;

  return (
    <ReactCSSTransitionGroup
      transitionName={{
        enter: style['example-enter'],
        enterActive: style['example-enter-active'],
        leave: style['example-leave'],
        leaveActive: style['example-leave-active'],
      }}
      transitionEnterTimeout={300}
      transitionLeaveTimeout={300}
    >
      <React.Fragment key={1}>
        <Backdrop open={show ? 1 : 0} onClick={clicked}  />
        <StyledModal>{children}</StyledModal>
      </React.Fragment>
    </ReactCSSTransitionGroup>
  );
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  show: PropTypes.bool.isRequired,
  clicked: PropTypes.func.isRequired,
};

export default Modal;

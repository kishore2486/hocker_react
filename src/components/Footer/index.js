/* eslint-disable react/jsx-one-expression-per-line */
import React from 'react';
import style from './index.module.css';

const currentYear = () => new Date().getFullYear();

const Footer = () => (
  <div className={`container-fluid ${style.footer}`}>
    <div className="row">
      <div className={`col ${style.fcol}`}>
        <h4>Links</h4>
        <ul>
          <li>About Us</li>
          <li>Login</li>
          <li>SignUp</li>
        </ul>
      </div>
      <div className={`col ${style.fcol}`}>
        <h4>Get Support</h4>
        <ul>
          <li>Contact Us</li>
          <li>Faq's</li>
          <li>Terms of service</li>
        </ul>
      </div>
      <div className={`col ${style.fcol}`}>
        <h4> © Copyrights</h4>
        <ul>
          <li>
            hocker.app © {currentYear()}
            <br />
            All rights reserved.
          </li>
        </ul>
      </div>
      <div className={`col ${style.fcol}`}>
        <h4>Hocker lite</h4>
      </div>
    </div>
  </div>
);

export default Footer;

/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
/* global google */

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.autocompleteInput = React.createRef();
    this.autocomplete = null;
    this.handlePlaceChanged = this.handlePlaceChanged.bind(this);
  }

  componentDidMount() {
    this.autocomplete = new google.maps.places.Autocomplete(this.autocompleteInput.current, { types: ['geocode'] });

    this.autocomplete.addListener('place_changed', this.handlePlaceChanged);
  }

  handlePlaceChanged() {
    const { onPlaceLoaded } = this.props;
    const place = this.autocomplete.getPlace();
    onPlaceLoaded(place);
  }

  render() {
    // const { mEdit } = this.props;
    const { handleChange, value } = this.props;

    return (
      <input
        ref={this.autocompleteInput}
        id="autocomplete"
        name="place"
        value={value}
        className="form-control"
        placeholder="Enter your address"
        onChange={handleChange}
        type="text"
      />
    );
  }
}

SearchBar.defaultProps = {
  // mEdit: false,
  onPlaceLoaded: () => {},
  handleChange: () => {},
  value: '',
  // value: '',
};

SearchBar.propTypes = {
  // mEdit: PropTypes.bool,
  onPlaceLoaded: PropTypes.func,
  handleChange: PropTypes.func,
  value: PropTypes.string,
  // value: PropTypes.string,
  // changedValue: PropTypes.string.isRequired,
};

export default SearchBar;

import React from 'react';
import PropTypes from 'prop-types';
import { StyledSideDrawer } from '../Styled/header';
import { Backdrop } from '../Styled/utils';

class SideDrawer extends React.Component {
  render() {
    const { show, clicked } = this.props;
    return (
      <React.Fragment>
        <StyledSideDrawer open={show ? 1 : 0}>
          <ul>
            <li>Login</li>
            <li>SignUp</li>
          </ul>
          
        </StyledSideDrawer>
        <Backdrop open={show ? 1 : 0} onClick={clicked} />
      </React.Fragment>
    );
  }
}

SideDrawer.propTypes = {
  show: PropTypes.bool.isRequired,
  clicked: PropTypes.func.isRequired,
};

export default SideDrawer;

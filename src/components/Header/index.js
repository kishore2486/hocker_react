/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { StyledHeader } from '../Styled/header';
import GoogleMap from '../GoogleMap';
import SideDrawer from './SideDrawer';
import { getAllShops } from '../../store/reducers/getAllShops';
import { userLogout } from '../../store/reducers/login';

class Header extends React.Component {
  state = {
    open: false,
    locationValue: '',
    show: false,
  };

  handleSideDrawer = () => {
    this.setState(prevState => ({
      open: !prevState.open,
    }));
  };

  onPlaceLoaded = place => {
    const { locationValue } = this.state;
    this.setState({
      locationValue: place.formatted_address || place.name,
    });
    if (place || locationValue) {
      this.props.history.push('/business/restaurants');
      this.props.getAllShops({ location: place.formatted_address || place.name || locationValue });
    }
  };

  toggleDropdown = e => {
    var elem = e.target;
    console.log(e.target);
    elem.contains('show') ? elem.remove('show') : elem.add('show');
  };

  handleChange = e => {
    const { value } = e.target;
    this.setState({
      locationValue: value,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { locationValue } = this.state;
    if (locationValue) {
      this.props.history.push('/business/restaurants');
      this.props.getAllShops({ location: locationValue });
    }
  };

  Logout = () => {
    this.setState({ logout: true });
    const { userLogout, history } = this.props;
    userLogout();
    setTimeout(() => {
      this.setState({ logout: false });
      history.push('/');
    }, 400);
  };

  render() {
    const { open, locationValue, show } = this.state;
    const { token } = this.props;

    return (
      <React.Fragment>
        <StyledHeader>
          <nav className="navbar navbar-expand-lg  flex-container mt-1  ">
            <div className="menu d-block d-lg-none" onClick={this.handleSideDrawer}>
              <div />
              <div />
              <div />
            </div>
            <Link to="/" className="navbar-brand logo ">
              Hocker
            </Link>
            <form className="form-inline  ml-auto" onSubmit={this.handleSubmit}>
              <div className="p-2 mt-1 search w-100 ">
                <span className="fas fa-search mx-3 "></span>
                <GoogleMap onPlaceLoaded={this.onPlaceLoaded} value={locationValue} handleChange={this.handleChange} />
              </div>
            </form>
            {token && (
              <div class="dropdown mr-5">
                <span role="presentation" onClick={() => this.setState(prevState => ({ show: !prevState.show }))}>
                  <i className="fa fa-user" aria-hidden="true"></i>
                </span>
                {show && (
                  <div
                    class="show"
                    style={{
                      backgroundColor: 'white',
                      boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.2)',
                      maxWidth: '120px',
                      marginTop: '10px',
                      position: 'absolute',
                      left: '-45px',
                      padding: '10px',
                      display: 'flex',
                      flexDirection: 'column',
                    }}
                  >
                    <Link to="/user/dashboard" className="text-center">
                      Dashboard
                    </Link>
                    <Link to="/" onClick={this.Logout} className="text-center">
                      Logout
                    </Link>
                  </div>
                )}
              </div>
            )}
          </nav>
        </StyledHeader>
        <SideDrawer show={open} clicked={this.handleSideDrawer} />
      </React.Fragment>
    );
  }
}

const Head = withRouter(Header);

const mapStateToProps = ({ user }) => ({
  ...user,
});

export default connect(
  mapStateToProps,
  { getAllShops, userLogout }
)(Head);

import styled from 'styled-components';

export const StyledHeader = styled.section`
  position: fixed;
  top: 0;
  z-index: 2;
  width:100%;
  height:80px;
  
  border-bottom: 3px solid yellow;
  background-color: white;

  .flex-container {
    
  }

  .flex-container > div {
    margin-top: 8px;
  }

  .menu > div {
    border: 2px solid wheat;
    margin: 0px 20px 8px 0;
    width: 38px;
  }

  .logo {
    font-size:22px;
    margin-left:5rem;
  }

  input[type='text'] {
    border: none;
    font-size: 18px;
    
    width:30rem;
    
    border-radius: 0 4px 4px 0;
    
    background: #f5f5f6;
    outline: none;
  }
  .search {
    border-radius: 0 4px 4px 0;
    background: #f5f5f6;
    height:40px;
    margin-right:10rem;
    
    
  }
  .fas {
    
    font-size: 15px;
    border-radius: 4px 0 0 4px;
    background: #f5f5f6;
  }

  @media (max-width: 640px) {
    .search {
      margin-right:0rem;
      
    }
    .fas {
      
      
     
    }
    .logo {
      font-size:20px;
      margin-left:1rem;
    }
  }

  @media (min-width: 990px) {
    // .flex-container {
    //   justify-content: space-around;
    // }
    // .menu {
    //   display: none;
    // }
    // .logo {
    //   flex-basis: 0px;
    // }
    // input[type='text'] {
    //   font-size: 16px;
    //   padding: 10px;
    //   width: 550px;
    //   position: relative;
    //   top: -1px;
    //   letter-spacing: 0.3px;
    // }
    // .fas {
    //   padding: 15px 10px;
    //   font-size: 14px;
    // }
  // }
`;

export const StyledSideDrawer = styled.div`
  position: fixed;
  width: 310px;
  max-width: 70%;
  height: 100%;
  left: 0;
  top: 0;
  z-index: 200;
  background-color: white;
  padding: 32px 16px;
  transition: transform 0.3s ease-out;
  transform: ${props => (props.open ? 'translateX(0)' : 'translateX(-100%)')};

  @media (min-width: 600px) {
    display: none;
  }
`;

import styled from 'styled-components';

export const Backdrop = styled.div`
  position: fixed;
  display: ${props => (props.open ? 'block' : 'none')};
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.25);
  z-index: 3;
`;

export const SpanButton = styled.span`
  text-align: center;
  display: inline-block;
  background-image: ${props =>
    props.signup
      ? 'linear-gradient(to right top, #229a2f, #20a639, #1db144, #16bd4f, #08c95a);'
      : 'linear-gradient(to right top, #343636, #353b39, #393f39, #414238, #4c4438);'};
  padding: 10px 14px;
  margin: 20px 8px 0 0px;
  border-radius: 25px;
  color: white;
  border: none;
  font-size: 15px;
  letter-spacing: 1.5px;
  font-weight: 700;
  transition: transform 0.2s ease-out;
  position: relative;
  cursor: pointer;

  &:hover {
    transform: translateY(-1px);
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.2);
  }

  @media (max-width: 600px) {
    margin-top: 35px;
    padding: 10px;
    font-size: 16px;
    width: 130px;
  }
`;

export const StyledModal = styled.div`
  position: fixed;
  z-index: 200;
  left: 15%;
  top: 10%;
  width: 300px;
  background-color: rgba(255, 255, 255, 1);
  border-radius: 6px;
  overflow: hidden;

  @media (min-width: 600px) {
    width: 800px;
    left: calc(41% - 250px);
  }
`;

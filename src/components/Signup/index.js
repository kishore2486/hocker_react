/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { userRegister } from '../../store/reducers/signup';
import Input from '../Form/input';

const mapStateToProps = ({ register }) => ({
  ...register,
});

class SignUp extends React.PureComponent {
  state = {
    email: '',
    emailError: false,
    password: '',
    passwordError: false,
    name: '',
    nameError: false,
    checked: false,
    business_type: null,
  };

  // shouldComponentUpdate(nextProps, nextState) {
  //   const { emailError, passwordError, checked } = this.state;
  //   return (
  //     nextState.emailError !== emailError || nextState.passwordError !== passwordError ||
  // nextState.checked !== checked
  //   );
  // }

  handleEmail = e => {
    return /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim.test(e.target.value)
      ? this.setState({
        emailError: false,
        email: e.target.value,
      })
      : this.setState({
        emailError: true,
      });
  };

  handlePassword = e => {
    return e.target.value.length >= 8 && e.target.value.length <= 16
      ? this.setState({
        passwordError: false,
        password: e.target.value,
      })
      : this.setState({
        passwordError: true,
      });
  };

  handleSwitch = e => {
    this.setState({
      checked: e.target.checked,
    });
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { userRegister } = this.props;
    const { password, passwordError, email, emailError, name, nameError, business_type } = this.state;
    if (!(passwordError && nameError && emailError)) {
      userRegister({ name, email, password, business_type });
    }
  };

  render() {
    const { email, emailError, password, passwordError, checked, name, nameError } = this.state;
    const { loading, loaded, isRegistered, error } = this.props;
    return (
      <form onSubmit={this.handleSubmit}>
        <Input
          label="Name"
          htmlFor="name"
          name="name"
          type="text"
          placeholder="Enter your name"
          value={name}
          change={this.handleChange}
          error={nameError ? 'Enter your valid email address' : ''}
        />
        <Input
          label="Email"
          htmlFor="email"
          type="email"
          placeholder="Enter your email"
          value={email}
          change={this.handleEmail}
          error={emailError ? 'Enter your valid email address' : ''}
        />
        <Input
          label="Password"
          htmlFor="password"
          type="password"
          placeholder="Enter your password"
          value={password}
          change={this.handlePassword}
          error={passwordError ? 'Password must be 8 to 10 characters' : ''}
        />
        <div style={{ marginBottom: '15px' }}>
          <div className="custom-control custom-switch">
            <input type="checkbox" className="custom-control-input" id="customSwitch1" onChange={this.handleSwitch} />
            <label className="custom-control-label" htmlFor="customSwitch1">
              Do you have business?
            </label>
          </div>{' '}
          {checked && (
            <select className="custom-select" name="business_type" onChange={this.handleChange}>
              <option selected>What kind of business do you have?</option>
              <option value="restaurant">Restaurant</option>
              <option value="book">book store</option>
              <option value="saloon">Saloon</option>
            </select>
          )}
        </div>
        <button type="submit" className="btn btn-success" disabled={loading}>
          {loading ? 'loading..' : 'SignUp'}
        </button>
        {loaded && !loading && (
          <div
            className={`alert ${isRegistered ? 'alert-success' : 'alert-danger'}`}
            style={{ marginTop: '15px' }}
            role="alert"
          >
            <span>
              {isRegistered && (
                <span>
                  Hurray! You've Registered Successfully.
                  <br /> Please check your email for verification.
                </span>
              )}
            </span>
          </div>
        )}
        {error.errors && (
          <div
            className={`alert ${isRegistered ? 'alert-success' : 'alert-danger'}`}
            style={{ marginTop: '15px' }}
            role="alert"
          >
            {error.errors.map(err => (
              <span>{err}</span>
            ))}
          </div>
        )}
      </form>
    );
  }
}

SignUp.defaultProps = {
  loading: false,
  loaded: false,
  isRegistered: false,
  error: {},
};

SignUp.propTypes = {
  loading: PropTypes.bool,
  loaded: PropTypes.bool,
  isRegistered: PropTypes.bool,
  error: PropTypes.object,
  userRegister: PropTypes.func.isRequired,
};

export default connect(
  mapStateToProps,
  { userRegister }
)(SignUp);

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { userVerify } from '../../store/reducers/verify';

class Verify extends React.PureComponent {
  componentDidMount() {
    const {
      match: {
        params: { token },
      },
      userVerify,
    } = this.props;
    console.log(token);
    if (token) userVerify({ token });
  }

  render() {
    const { loaded, loading, isVerified, error } = this.props;
    return (
      <div className="col-sm-6" style={{ marginTop: '80px' }}>
        <div className="text-center mt-4">
          {loading && (
            <span>
              <i
                className="fas fa-cog fa-4x fa-spin mb-2"
                style={{ fontSize: '60px', color: 'blue' }}
                aria-hidden="true"
              />
              <p className="mt-3">Verification in Progress..</p>
            </span>
          )}
          {loaded && isVerified && (
            <span>
              <i
                className="far fa-check-circle fa-4x mb-2"
                style={{ fontSize: '60px', color: 'blue' }}
                aria-hidden="true"
              />
              <p className="mt-4 mb-1">Verfied !</p>
              <p className="mt-2">
                You can{' '}
                <Link className="text-danger" to="/">
                  Login now
                </Link>
              </p>
            </span>
          )}
          {error && loaded && !isVerified && (
            <span>
              <i
                className="fas fa-exclamation-circle fa-4x mb-2"
                style={{ fontSize: '60px', color: 'red' }}
                aria-hidden="true"
              />
              <p className="mt-4 mb-1">Unable to verify your Email.</p>
              <p>Please contact support.</p>
            </span>
          )}
        </div>
      </div>
    );
  }
}

Verify.defaultProps = {
  loaded: false,
  loading: false,
  isVerified: false,
  error: '',
};

Verify.propTypes = {
  loaded: PropTypes.bool,
  loading: PropTypes.bool,
  isVerified: PropTypes.bool,
  error: PropTypes.string,
  match: PropTypes.object.isRequired,
  userVerify: PropTypes.func.isRequired,
};

const mapStateToProps = ({ verify }) => ({
  ...verify,
});

export default connect(
  mapStateToProps,
  { userVerify }
)(Verify);

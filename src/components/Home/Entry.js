/* eslint-disable */
import React, { useState, useEffect } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { connect } from 'react-redux';
import style from './index.module.css';
import { SpanButton } from '../Styled/utils';
import HomeImg from '../../assests/home.png';
import FormWrapper from '../Form/wrapper';

const WORDS = [
  { id: 0, text: 'Do you have business?' },
  { id: 1, text: 'Are you struggling to show off' },
  { id: 2, text: 'Explore your business in the world.' },
  { id: 3, text: 'Get Satisfied!' },
];

const Entry = props => {
  const [index, setIndex] = useState(0);
  const [open, setModal] = useState(false);

  useEffect(() => {
    let intervel;
    (() => {
      intervel = setInterval(() => setIndex(current => (current + 1) % WORDS.length), 2500);
    })();
    return () => {
      clearInterval(intervel);
    };
  }, []);

  const toggle = () => {
    setModal(current => !current);
  };

  const { token } = props;

  return (
    <React.Fragment>
      <section className={style.entry}>
        <div className="container-fluid">
          <div className="row">
            <div className={`col ${style.text}`}>
              <ReactCSSTransitionGroup
                transitionName={{
                  enter: style['example-enter'],
                  enterActive: style['example-enter-active'],
                  leave: style['example-leave'],
                  leaveActive: style['example-leave-active'],
                }}
                transitionEnterTimeout={300}
                transitionLeaveTimeout={300}
              >
                <h2 key={index}>{WORDS[index].text}</h2>
              </ReactCSSTransitionGroup>
              <h6>
                <strong>SignUp </strong>
                for Free to Promote your business.
              </h6>
              {!token && (
                <div className={style.links}>
                  <SpanButton onClick={toggle}>Login | SignUp</SpanButton>
                </div>
              )}
            </div>
            <div className={`col ${style.homepic}`}>
              <img src={HomeImg} alt="hocker promote business" />
            </div>
          </div>
        </div>
      </section>
      <FormWrapper show={open} clicked={toggle} />
    </React.Fragment>
  );
};

const mapStateToProps = ({ user }) => ({
  ...user,
});

export default connect(
  mapStateToProps,
  null
)(Entry);

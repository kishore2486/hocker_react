import React from 'react';
import illus1 from '../../assests/illus2.png';
import illus2 from '../../assests/illus3.png';
import illus3 from '../../assests/illus1.png';
import style from './index.module.css';

const Features = () => (
  <div className={`container-fluid ${style.feature}`}>
    <div className="row">
      <div className="col">
        <img src={illus1} alt="features of hocker" />
        <h4>Create an account</h4>
        <p>SignUp for Free to promote your business.</p>
      </div>
      <div className="col">
        <img src={illus2} alt="features of hocker" />
        <h4>Store your Items</h4>
        <p>Add your products in your account.</p>
      </div>
      <div className="col">
        <img src={illus3} alt="features of hocker" />
        <h4>Create an account</h4>
        <p>SignUp for Free to promote your business.</p>
      </div>
    </div>
  </div>
);

export default Features;

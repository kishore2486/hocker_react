import React from 'react';
import Entry from './Entry';
import Features from './features';

const Home = () => {
  return (
    <React.Fragment>
      <Entry />
      <Features />
    </React.Fragment>
  );
};

export default Home;

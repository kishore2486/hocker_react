/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import Img1 from '../../assests/eat.jpg';
import Img2 from '../../assests/home.png';
import Img3 from '../../assests/store.jpg';
import Img4 from '../../assests/signup.jpeg';
import styles from './index.module.css';
import Modal from '../Modal';

class Gallery extends React.Component {
  state = {
    GalM: false,
  };

  handleGalM = () => {
    this.setState(prevState => ({
      GalM: !prevState.GalM,
    }));
  };

  render() {
    const Imgs = [Img1, Img2, Img3, Img4];
    const { GalM } = this.state;

    return (
      <div className="container">
        <button type="button" className="btn btn-info" onClick={this.handleGalM}>
          &#43; Add Photos of your Business
        </button>
        <br />
        <br />

        <div className={`${styles.gallery} row mx-auto`}>
          {Imgs.map((ele, index) => (
            <div key={String(index)} className=" col-lg-4 col-md-6 col-sm-6 col-12 p-1 my-2 ">
              <button type="button" className={`btn-sm btn-danger p-2 ${styles.galDel} `}>
                Delete
              </button>
              <img src={ele} alt="gallery..." />
            </div>
          ))}
        </div>
        {
          <Modal show={GalM} clicked={this.handleGalM}>
            <form style={{ padding: '20px' }}>
              <div className="form-group">
                <label htmlFor="file">On Above Price:</label>
                <input type="file" name="file" className="form-control" id="file" />
              </div>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>{' '}
              <button type="button" className="btn btn-danger" onClick={this.handleGalM}>
                Cancel
              </button>
            </form>
          </Modal>
        }
      </div>
    );
  }
}

export default Gallery;

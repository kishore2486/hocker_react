/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import styles from './index.module.css';
import EditImg from '../../assests/edit.png';
import GoogleMap from '../GoogleMap';
import { getShopDetails } from '../../store/reducers/shopDetails';
import { userLogout } from '../../store/reducers/login';
import { updateProfile } from '../../store/reducers/updateProfile';
import { addDetails } from '../../store/reducers/addDetails';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.textEmail = React.createRef();
    this.textPass = React.createRef();
  }

  state = {
    editEmail: '',
    password: 'something...',
    edit: [],
    place: '',
  };

  componentDidMount() {
    const { getShopDetails, id } = this.props;
    getShopDetails(id);
  }

  handleChange = e => {
    const { name, value } = e.target;
    const { edit } = this.state;
    if (edit.length > 0) {
      this.setState({
        [name]: value,
      });
    }
  };

  OnEditClick = param => () => {
    const { edit: prevEdit } = this.state;
    this.setState(
      {
        edit:
          prevEdit.filter(ele => ele === param).length > 0
            ? prevEdit.filter(ele => ele !== param)
            : [...prevEdit, param],
      },
      () => {
        if (param === 'email') {
          this.textEmail.current.focus();
        } else if (param === 'password') {
          this.textPass.current.focus();
        }
      }
    );
  };

  Logout = () => {
    this.setState({ logout: true });
    const { userLogout, history } = this.props;
    userLogout();
    setTimeout(() => {
      this.setState({ logout: false });
      history.push('/');
    }, 400);
  };

  onPlaceLoaded = place => {
    this.setState({
      place: place.formatted_address,
    });
  };

  handleFileUpload = async e => {
    let file = e.target.files;
    const data = new FormData();
    data.append('file', file[0]);
    const { getShopDetails, id, updateProfile } = this.props;
    const res = await updateProfile(data);
    if (res) getShopDetails(id);
  };

  handleSubmit = e => {
    e.preventDefault();
    const { addDetails } = this.props;
    const { email, place } = this.state;
    if (email || place) {
      addDetails({ location: place });
    }
  };

  render() {
    const { editEmail, password, edit, place, logout } = this.state;
    const { email, shopDetails, name, addDetailsReq } = this.props;
    const ratingMap =
      shopDetails && shopDetails.data && shopDetails.data[0] && shopDetails.data[0].User
        ? shopDetails.data[0].User.ratings
        : [];
    let rating = ratingMap.length > 0 ? ratingMap.reduce((a, c) => a.rating + c.rating) : 0;
    if (rating.rating) {
      rating = rating.rating / ratingMap.length;
    } else {
      rating = rating / ratingMap.length;
    }

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-4">
            <div className={`${styles.ppic}`}>
              {shopDetails && shopDetails.data.length > 0 && shopDetails.data[0].image_url ? (
                <>
                  <img src={shopDetails.data[0].image_url} alt="profile pic of hocker" />
                  <p className={`${styles.imgDes}`}>&#43; change picture</p>
                </>
              ) : (
                <form encType="multipart/form-data">
                  <input type="file" name="file" onChange={this.handleFileUpload} placeholder="Upload Pic" />
                </form>
              )}
            </div>
            <div className={`${styles.des}`}>
              <p>Name: {name}</p>
              <p>
                Rating: {rating} <span style={{ fontSize: '120%', color: 'green' }}>&#9733;</span>
              </p>
            </div>
          </div>
          <div className="col-8">
            <div className="form-group">
              <label htmlFor="exampleInput1">Email</label>
              <div className={styles.input}>
                <input
                  type="email"
                  className="form-control"
                  name="editEmail"
                  id="exampleInput1"
                  value={!edit.includes('email') ? email : editEmail}
                  disabled={!edit.includes('email')}
                  onChange={this.handleChange}
                  ref={this.textEmail}
                />
                <div className={styles.editDiv} role="presentation" onClick={this.OnEditClick('email')}>
                  <img src={EditImg} alt="something ... " />
                  <div className={styles.toolTip}>Edit</div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInput2">Password</label>
              <div className={styles.input}>
                <input
                  type="password"
                  name="password"
                  disabled={!edit.includes('password')}
                  onChange={this.handleChange}
                  className="form-control"
                  id="exampleInput2"
                  value={password}
                  ref={this.textPass}
                />
                <div className={styles.editDiv} role="presentation" onClick={this.OnEditClick('password')}>
                  <img src={EditImg} alt="something ... " />
                  <div className={styles.toolTip}>Edit</div>
                </div>
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="exampleInput3">Address:</label>
              <div className={styles.input}>
                {!edit.includes('map') && (
                  <input
                    type="text"
                    name="place"
                    disabled={!edit.includes('map')}
                    className="form-control"
                    id="exampleInput3"
                    value={shopDetails && shopDetails.data.length > 0 && shopDetails.data[0].location}
                  />
                )}
                {edit.includes('map') && (
                  <GoogleMap value={place} handleChange={this.handleChange} onPlaceLoaded={this.onPlaceLoaded} />
                )}
                <div className={styles.editDiv} role="presentation" onClick={this.OnEditClick('map')}>
                  <img src={EditImg} alt="something ... " />
                  <div className={styles.toolTip}>Edit</div>
                </div>
              </div>
            </div>
            <button type="submit" className="btn btn-primary" onClick={this.handleSubmit} disabled={edit.length <= 0}>
              {addDetailsReq.loading && 'updating..'}
              {addDetailsReq.loaded && 'Updated'}
              {!addDetailsReq.loading && !addDetailsReq.loaded && 'Save'}
            </button>{' '}
            <button type="button" className="btn btn-danger" onClick={this.Logout}>
              {logout ? 'logging out..' : 'Logout'}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user, shopDetails, addDetails, updateProfilePic }) => ({
  ...user,
  ...shopDetails,
  addDetailsReq: { ...addDetails },
  updateProfilePic: { ...updateProfilePic },
});

export default connect(
  mapStateToProps,
  { getShopDetails, userLogout, updateProfile, addDetails }
)(withRouter(Profile));

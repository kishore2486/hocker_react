/* eslint-disable */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Profile from './profile';
import AddItems from './addItems';
import Offers from './Offers';
import styles from './index.module.css';

class UsableDashboard extends PureComponent {
  state = {
    id: 0,
  };

  componentDidMount() {
    const { business_type, history } = this.props;
    if (!business_type) {
      history.push('/');
    }
  }

  handleComp = id => () => {
    this.setState({
      id,
    });
  };

  render() {
    const { id } = this.state;

    const list = [
      { id: 0, name: 'Profile', comp: <Profile /> },
      { id: 1, name: 'Add Items', comp: <AddItems /> },
      { id: 2, name: 'Offers', comp: <Offers /> },
    ];

    return (
      <div className={`${styles.restaurant} `}>
        <div className="container" style={{ position: 'sticky' }}>
          <div className="row">
            <div className={` col-md-3 d-none d-md-block ${styles.links} fixed`}>
              <ul>
                {list.map(ele => (
                  <li
                    role="presentation"
                    key={ele.id}
                    className={ele.id === id ? `${styles.LiActive}` : null}
                    onClick={this.handleComp(ele.id)}
                  >
                    {ele.name}
                  </li>
                ))}
              </ul>
            </div>
            <div className={` ${styles.content}  col-md-9 py-4`}>{list[id].comp}</div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  ...user,
});

export default connect(
  mapStateToProps,
  null
)(UsableDashboard);

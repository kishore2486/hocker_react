/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import { addOffers } from '../../store/reducers/addOffers';
import { getOffers } from '../../store/reducers/getOffers';
import Modal from '../Modal';

class Offers extends React.Component {
  state = {
    offM: false,
    offDet: {},
  };

  componentDidMount() {
    const { getOffers } = this.props;
    getOffers();
  }

  componentDidUpdate(prevProps, prevState) {
    const { offM } = prevState;
    const { getOffers } = this.props;
    const { offM: modal } = this.state;
    if (offM !== modal && !modal) {
      getOffers();
    }
  }

  handleOffM = () => {
    this.setState(prevState => ({
      offM: !prevState.offM,
    }));
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState(prevState => ({
      offDet: {
        ...prevState.offDet,
        [name]: value,
      },
    }));
  };

  handleSubmit = e => {
    e.preventDefault();
    const { offDet } = this.state;
    const { addOffers, loaded } = this.props;
    if (Object.keys(offDet).length === 4) {
      addOffers({ ...offDet });
      if (loaded) setTimeout(() => this.setState({ offM: false }), 700);
    }
  };

  render() {
    const { offM, offDet } = this.state;
    const { loading, loaded, error, data } = this.props;

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <button type="button" className="btn btn-success" onClick={this.handleOffM}>
              &#43; Add Offers
            </button>
            <br />
            <br />
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Id</th>
                  <th scope="col">Name</th>
                  <th scope="col">Discount</th>
                  <th scope="col">On Above price</th>
                  <th scope="col">Expires</th>
                </tr>
              </thead>
              <tbody>
                {data.length > 0 &&
                  data.map((ele, i) => (
                    <tr key={String(i)}>
                      <th scope="row">{i + 1}</th>
                      <td>{ele.name}</td>
                      <td>{ele.discount}</td>
                      <td>{ele.on_above_price}</td>
                      <td>{ele.expires}</td>
                    </tr>
                  ))}
              </tbody>
            </table>
            {
              <Modal show={offM} clicked={this.handleOffM}>
                <form style={{ padding: '20px' }}>
                  <div className="form-group">
                    <label htmlFor="name">Name:</label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      name="name"
                      value={offDet.name}
                      placeholder="something like Fesitval Offer..."
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="discount">Discount:</label>
                    <input
                      type="number"
                      className="form-control"
                      id="discount"
                      name="discount"
                      value={offDet.discount}
                      placeholder="In terms Of percentage..."
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="Price">On Above Price:</label>
                    <input
                      type="number"
                      className="form-control"
                      name="on_above_price"
                      id="Price"
                      value={offDet.on_above_price}
                      placeholder="on_above_price"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="expire">Expires On:</label>
                    <input
                      type="date"
                      className="form-control"
                      name="expires"
                      id="expires"
                      value={offDet.expires}
                      onChange={this.handleChange}
                    />
                  </div>
                  <button type="submit" className="btn btn-primary" onClick={this.handleSubmit}>
                    {loading && 'loading..'}
                    {loaded && 'Data Updated.'}
                    {!loading && !loaded && 'Submit'}
                  </button>{' '}
                  <button type="button" className="btn btn-danger" onClick={this.handleOffM}>
                    Cancel
                  </button>
                  {error && (
                    <div className={`alert ${'alert-danger'}`} style={{ marginTop: '15px' }} role="alert">
                      <span>{error.msg}</span>
                    </div>
                  )}
                </form>
              </Modal>
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ addOffers, getOffers }) => ({
  ...addOffers,
  data: getOffers.data,
});

export default connect(
  mapStateToProps,
  { addOffers, getOffers }
)(Offers);

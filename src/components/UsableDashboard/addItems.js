/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import styles from './index.module.css';
import Modal from '../Modal';
import { getCategories } from '../../store/reducers/getCategories';
import { addCategory, clearCreateCategories } from '../../store/reducers/addCategory';

class AddItems extends React.Component {
  state = {
    catM: false,
    itemM: false,
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value,
    });
  };

  handleFileUpload = e => {
    let file = e.target.files;
    this.setState({
      file: file[0],
    });
  };

  handleCatM = () => {
    this.setState(prevState => ({
      catM: !prevState.catM,
    }));
  };

  handleItemM = param => e => {
    this.setState(prevState => ({
      itemM: !prevState.itemM,
      category_name: param,
    }));
  };

  componentDidMount() {
    const { getCategories } = this.props;
    getCategories();
  }

  componentDidUpdate(prevProps, prevState) {
    const {
      uploading: { loaded: PrevLoaded },
    } = prevProps;
    const {
      uploading: { loaded },
      getCategories,
      clearCreateCategories,
    } = this.props;
    if (PrevLoaded !== loaded && loaded) {
      getCategories();
      setTimeout(() => {
        this.setState({ catM: false, itemM: false });
        clearCreateCategories();
      }, 1300);
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    const { category_name, item_name, file, price } = this.state;
    const { addCategory } = this.props;
    if (category_name && item_name && file && price) {
      const data = new FormData();
      data.append('file', file);
      data.append('category_name', category_name);
      data.append('item_name', item_name);
      data.append('price', price);
      addCategory(data);
    }
  };

  render() {
    const { types, catM, itemM } = this.state;
    const {
      categories: category,
      uploading: { loading, loaded },
    } = this.props;
    const categories = category && category.categories && Object.keys(category.categories);

    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <button type="button" className="btn btn-success" onClick={this.handleCatM}>
              &#43; Add Categories
            </button>
            <br />
            <br />
            {categories &&
              categories.length > 0 &&
              categories.map(ele => (
                <React.Fragment>
                  <div className={styles.catIt}>
                    <h5>{ele}:</h5>
                    <div>
                      <button type="button" className="btn btn-primary" onClick={this.handleItemM(ele)}>
                        &#43; Add Items
                      </button>{' '}
                      <button type="button" className="btn btn-danger">
                        Delete
                      </button>
                    </div>
                  </div>
                  <br />
                  <table className="table">
                    <thead>
                      <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Category</th>
                        <th scope="col">Name</th>
                        <th scope="col">Img</th>
                        <th scope="col">Price</th>
                        <th scope="col">Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      {category.categories[ele].map(item => (
                        <tr>
                          <th scope="row">1</th>
                          <td>{item.category_name}</td>
                          <td>{item.item_name}</td>
                          <td>
                            <img src={item.image_url} style={{ width: '50px', height: '50px' }} alt="items" />
                          </td>
                          <td>{item.price} &#8377;</td>
                          <td>
                            <button type="button" className="btn-sm btn-danger">
                              Delete
                            </button>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </React.Fragment>
              ))}

            {
              <Modal show={catM} clicked={this.handleCatM}>
                <form style={{ padding: '20px' }} onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label htmlFor="category">Category Type:</label>
                    <input
                      type="text"
                      className="form-control"
                      id="category"
                      name="category_name"
                      onChange={this.handleChange}
                      placeholder="something like veg, non-veg ..."
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="name">Name:</label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      name="item_name"
                      onChange={this.handleChange}
                      placeholder="something like dosa, ghee dosa.."
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="Price">Price:</label>
                    <input
                      type="number"
                      name="price"
                      onChange={this.handleChange}
                      className="form-control"
                      id="Price"
                      placeholder=""
                    />
                  </div>
                  <div className="form-group">
                    <input type="file" name="file" onChange={this.handleFileUpload} placeholder="Upload Pic" />
                  </div>
                  <button type="submit" className="btn btn-primary">
                    {loading ? 'loading..' : loaded ? 'Data Updated!' : 'Submit'}
                  </button>{' '}
                  <button type="button" className="btn btn-danger" onClick={this.handleCatM}>
                    Cancel
                  </button>
                </form>
              </Modal>
            }
            {
              <Modal show={itemM} clicked={this.handleItemM}>
                <form style={{ padding: '20px' }} onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <label htmlFor="name">Name:</label>
                    <input
                      type="text"
                      name="item_name"
                      onChange={this.handleChange}
                      className="form-control"
                      id="name"
                      placeholder="something like dosa, ghee dosa.."
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="Price">Price:</label>
                    <input
                      type="number"
                      name="price"
                      onChange={this.handleChange}
                      className="form-control"
                      id="Price"
                      placeholder=""
                    />
                  </div>
                  <div className="form-group">
                    <input type="file" name="file" onChange={this.handleFileUpload} placeholder="Upload Pic" />
                  </div>
                  <button type="submit" className="btn btn-primary">
                    {loading ? 'loading..' : loaded ? 'Data Updated!' : 'Submit'}
                  </button>{' '}
                  <button type="button" className="btn btn-danger" onClick={this.handleItemM}>
                    Cancel
                  </button>
                </form>
              </Modal>
            }
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ categories, createCategory }) => ({
  ...categories,
  uploading: { ...createCategory },
});

export default connect(
  mapStateToProps,
  { getCategories, addCategory, clearCreateCategories }
)(AddItems);

/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';

const Input = ({ label, htmlFor, type, name, placeholder, error, change }) => {
  const InputClassName = error ? 'is-invalid' : null;
  return (
    <div className="form-group">
      <label htmlFor={htmlFor}>{label}</label>
      <input
        type={type}
        name={name}
        className={`form-control ${InputClassName}`}
        id={htmlFor}
        placeholder={placeholder}
        onChange={change}
      />
      {error && <div className="invalid-feedback">{error}</div>}
    </div>
  );
};

Input.defaultProps = {
  name: '',
};

Input.propTypes = {
  label: PropTypes.string.isRequired,
  htmlFor: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  change: PropTypes.func.isRequired,
  error: PropTypes.string.isRequired,
  name: PropTypes.string,
};

export default Input;

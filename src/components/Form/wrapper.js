import React from 'react';
import PropTypes from 'prop-types';
import Modal from '../Modal';
import ModalImg from '../../assests/login.jpg';
import Login from '../Login';
import SignUP from '../Signup';
import styles from './wrapper.module.css';

class FormWrapper extends React.Component {
  state = {
    click: true
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { show } = this.props;
    const { click } = this.state;
    return show !== nextProps.show || click !== nextState.click;
  }

  handleClick = () => {
    this.setState(prevState => ({
      click: !prevState.click
    }))
  }


  render() {
    const { show, clicked } = this.props;
    const { click } = this.state;
    const style = { borderBottom: '2px solid green' };
    return (
      <Modal show={show} clicked={clicked}>
        <div className="container-fluid">
          <div className="row">
            <div className="col-6" style={{ paddingLeft: '0px' }}>
              <img
                src={ModalImg}
                style={{ width: '100%', height: '600px' }}
                alt="hocker login page"
              />
            </div>
            <div className="col-6">
              <div className={styles.buttons}>
                <button
                  type="button"
                  onClick={this.handleClick}
                  style={click ? style : {}}>
                  Login
                </button>
                <button
                  type="button"
                  onClick={this.handleClick}
                  style={click ? {} : style}>
                  SignUP
                </button>
              </div>
              {click ? (<Login />) : (<SignUP />)}
            </div>
          </div>
        </div>
      </Modal>
    );
  }
};

FormWrapper.propTypes = {
  show: PropTypes.bool.isRequired,
  clicked: PropTypes.func.isRequired,
};

export default FormWrapper;

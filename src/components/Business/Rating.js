import React from 'react';
import PropTypes from 'prop-types';

const MakeArray = (len, val = undefined) => new Array(len).fill(val);

const Rating = ({ rating }) => (
  <>
    {rating.toString()[1] ? rating : `${rating}.0`}{' '}
    {MakeArray(Math.round(rating)).map((item, ind) => (
      <i key={String(ind)} className="fas fa-star"></i>
    ))}
    {rating.toString().split('.')[1] ? <i className="fas fa-star-half-alt"></i> : ''}
    {MakeArray(5 - (rating.toString().split('.')[1] ? Math.round(rating) + 1 : rating)).map((item, ind) => (
      <i key={String(ind)} className="far fa-star"></i>
    ))}
  </>
);

Rating.propTypes = {
  rating: PropTypes.number.isRequired,
};

export default Rating;

/* eslint-disable  */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Image from './Image';
import Rating from './Rating';
import ShimmeringImage from './ShimmeringImage';
import styles from './index.module.css';

class Shops extends React.Component {
  render() {
    const { shops } = this.props;
    const { name } = this.props;
    const list = shops && shops.data ? Object.keys(shops.data) : [];
    console.log(list);
    return (
      <div className="col-9">
        <div className="mx-3">
          <h4>{name}</h4>
        </div>
        <div className="row">
          {list.length > 0 &&
            list.map(ele =>
              shops.data[ele].map((res, index) => {
                return (
                  <div className="col-12 col-sm-6 col-md-4 mb-4" key={String(index)}>
                    <Link to={`/business/${name}/${res.User.id}`}>
                      <div className={`card ${styles.Card}`}>
                        <ShimmeringImage imgUrl={res.image_url || ''}>
                          {(src, loading) => <Image src={src} loading={loading} />}
                        </ShimmeringImage>
                        <div className={`card-body ${styles.cardBody}`}>
                          <h6>{res.User.name}</h6>
                          <div className={styles.rating}>
                            <Rating rating={res.User.ratings.length > 0 ? res.User.ratings[0].rating : 0} />
                          </div>
                        </div>
                      </div>
                    </Link>
                  </div>
                );
              })
            )}
        </div>
      </div>
    );
  }
}

Shops.propTypes = {
  name: PropTypes.string.isRequired,
};

const mapStateToProps = ({ allShops }) => ({
  ...allShops,
});

export default connect(
  mapStateToProps,
  null
)(Shops);

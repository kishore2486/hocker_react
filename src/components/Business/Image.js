import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.module.css';

const Image = ({ loading, src }) => (
  <>
    {loading && <div className={`card-img-top p-2 ${styles.cardImg} ${styles.shimmer}`}></div>}
    {!loading && <img className={`card-img-top p-2 ${styles.cardImg}`} src={src} alt="profile"></img>}
  </>
);

Image.propTypes = {
  loading: PropTypes.bool.isRequired,
  src: PropTypes.string.isRequired,
};

export default Image;

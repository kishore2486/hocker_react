/* eslint-disable */
import React from 'react';
import { connect } from 'react-redux';
import styles from './index.module.css';
import Image from './Image';
import ShimmeringImage from './ShimmeringImage';
import { getShopDetails } from '../../store/reducers/shopDetails';

class ShopDetails extends React.Component {
  componentDidMount() {
    const {
      getShopDetails,
      match: {
        params: { id },
      },
    } = this.props;
    if (id) {
      getShopDetails(id);
    }
  }

  render() {
    const { shopDetails } = this.props;
    let location = '';
    let name = '';
    let rating = 0;
    let categories = [];
    let image_url = '';
    if (shopDetails && shopDetails.data && shopDetails.data.length > 0) {
      location = shopDetails.data[0].location;
      name = shopDetails.data[0].User.name;
      rating = shopDetails.data[0].User.ratings.length > 0 ? shopDetails.data[0].User.ratings[0].rating : 0;
      categories = shopDetails.data[0].User.categories;
      image_url = shopDetails.data[0].image_url;
    }

    const sectionStyle = {
      width: '100%',
      height: '215px',
      backgroundImage: `url(${image_url})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      paddingBottom: 5,
      backgroundPosition: 'center',
    };

    return (
      <div className="">
        <div className={` container py-5 mx-0  col-12 sticky-top ${styles.bgBanner}`} style={sectionStyle}>
          <div className={styles.overlay}></div>
          <div className=" row ">
            <div className="col-lg-4 text-center">
              <div className="">
                <img src={image_url} alt="resturant Banner" className="rounded-sm" height="180" />
              </div>
            </div>
            <div className="col-lg-6">
              <h3 className="text-white">{name}</h3>
              <p className="text-light my-1">American, North Indian, Chinese, Italian, Desserts, Beverages</p>
              <p className="text-light my-1">{location}</p>
              <ul className={`nav text-white mt-3 align-middle ${styles.blockDesign}`}>
                <li className="nav-item b">
                  <i className="fas fa-star"> {rating}</i>
                  <p>Rated by 1000</p>
                </li>
                <li className="nav-item b text-center">
                  Open At <p>9:00 am</p>
                </li>
                <li className="nav-item b text-center">
                  Close At<p>4:00 am </p>
                </li>
              </ul>
            </div>
            <div className={`col-lg-12 bg-white py-3 ${styles.spacer}`}></div>{' '}
          </div>
        </div>
        <div className="col-lg-12 bg-white">
          <div className="row ">
            <div className="col-2 p-0 ml-2 bg-white border-right-0  border-dark">
              <ul className={`list-group w-100 ${styles.category}`}>
                {categories.map(ele => {
                  return (
                    <li key={ele.id} className={`list-group-item`}>
                      {ele.category_name}
                    </li>
                  );
                })}
              </ul>
            </div>
            <div className="col-lg-9 mt-3">
              <div className="container-fluid">
                <div className="row">
                  {categories.map(ele => {
                    return (
                      <div key={ele.id} className={`col-lg-3  ${styles.dish}`}>
                        <div className="mt-3">
                          <ShimmeringImage imgUrl={ele.image_url}>
                            {(src, loading) => <Image src={src} loading={loading} />}
                          </ShimmeringImage>
                          <div className="card-body">
                            <h5 className="h6">{ele.item_name}</h5>
                            <h5 className="h6">$ {ele.price}</h5>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ shopDetails }) => ({
  ...shopDetails,
});

export default connect(
  mapStateToProps,
  { getShopDetails }
)(ShopDetails);

/* eslint-disable */
import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import Propre from './Propre';
import styles from './index.module.css';

class Business extends React.Component {
  state = {
    id: 0,
  };

  handleComp = id => () => {
    this.setState({
      id,
    });
  };

  render() {
    const { shops } = this.props;
    const list = shops && shops.data ? Object.keys(shops.data) : [];
    const { id } = this.state;
    const {
      match: {
        params: { name },
      },
    } = this.props;

    return (
      <div className={`container  col-12  ${styles.business}`}>
        <div className="row">
          <div className="col-3 col-md-3 col-sm-1 d-none d-lg-block d-md-block">
            <div className={styles.shops}>
              <ul>
                {list.length > 0 &&
                  list.map((ele, index) => (
                    <Link key={index} to={`/business/${ele}`}>
                      <li
                        role="presentation"
                        className={index === id ? `${styles.BLiActive}` : null}
                        onClick={this.handleComp(index)}
                      >
                        {ele}
                      </li>
                    </Link>
                  ))}
              </ul>
            </div>
          </div>
          {name && <Propre name={name} />}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ allShops }) => ({
  ...allShops,
});

export default connect(
  mapStateToProps,
  null
)(Business);

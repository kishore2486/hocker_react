import React from 'react';
import PropTypes from 'prop-types';

class ShimmeringImage extends React.Component {
  state = {
    imgUrl: '',
    loading: true,
  };

  componentDidMount() {
    const { imgUrl } = this.props;
    const imageLoader = new Image();
    imageLoader.src = imgUrl;

    imageLoader.onload = () => this.setState({ imgUrl, loading: false });
  }

  render() {
    const { imgUrl, loading } = this.state;
    const { children } = this.props;
    return <React.Fragment>{children(imgUrl, loading)}</React.Fragment>;
  }
}

ShimmeringImage.propTypes = {
  imgUrl: PropTypes.string.isRequired,
  children: PropTypes.elementType.isRequired,
};

export default ShimmeringImage;

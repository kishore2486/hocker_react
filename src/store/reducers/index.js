/* eslint-disable */
import { combineReducers } from 'redux';
import register from './signup';
import verify from './verify';
import login from './login';
import { user } from './login';
import shopDetails from './shopDetails';
import categories from './getCategories';
import allShops from './getAllShops';
import updateProfilePic from './updateProfile';
import createCategory from './addCategory';
import addDetails from './addDetails';
import addOffers from './addOffers';
import getOffers from './getOffers';

export default combineReducers({
  register,
  verify,
  login,
  user,
  shopDetails,
  categories,
  allShops,
  updateProfilePic,
  createCategory,
  addDetails,
  addOffers,
  getOffers,
});

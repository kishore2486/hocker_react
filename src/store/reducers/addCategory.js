import { ADD_CATEGORY_API } from '../apis';
import Client from '../../axios';

const ADD_CATEGORY = 'ADD_CATEGORY';
const ADD_CATEGORY_SUCCESS = 'ADD_CATEGORY_SUCCESS';
const ADD_CATEGORY_ERROR = 'ADD_CATEGORY_ERROR';

const initialState = {
  loading: false,
  loaded: false,
  isRegistered: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_CATEGORY:
      return {
        ...state,
        loading: true,
        error: {},
      };
    case ADD_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
      };
    case ADD_CATEGORY_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    case 'CLEAR_ADD_CATEGORIES':
      return { ...initialState };
    default:
      return state;
  }
}

export const addCategory = data => async dispatch => {
  dispatch({ type: ADD_CATEGORY });
  try {
    await Client.post(ADD_CATEGORY_API, data);
    dispatch({ type: ADD_CATEGORY_SUCCESS });
  } catch (error) {
    dispatch({ type: ADD_CATEGORY_ERROR, error });
  }
};

export const clearCreateCategories = () => dispatch => dispatch({ type: 'CLEAR_ADD_CATEGORIES' });

import { ADD_DETAILS_API } from '../apis';
import Client from '../../axios';

const ADD_DETAILS = 'ADD_DETAILS';
const ADD_DETAILS_SUCCESS = 'ADD_DETAILS_SUCCESS';
const ADD_DETAILS_ERROR = 'ADD_DETAILS_ERROR';

const initialState = {
  loading: false,
  loaded: false,
  isRegistered: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_DETAILS:
      return {
        ...state,
        loading: true,
        error: {},
      };
    case ADD_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
      };
    case ADD_DETAILS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const addDetails = data => async dispatch => {
  dispatch({ type: ADD_DETAILS });
  try {
    const res = await Client.post(ADD_DETAILS_API, data);
    if (res) dispatch({ type: ADD_DETAILS_SUCCESS });
  } catch (error) {
    dispatch({ type: ADD_DETAILS_ERROR, error });
  }
};

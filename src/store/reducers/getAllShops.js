import { GET_ALL_SHOPS_API } from '../apis';
import Client from '../../axios';

const GET_SHOPS = 'GET_SHOPS';
const GET_SHOPS_SUCCESS = 'GET_SHOPS_SUCCESS';
const GET_SHOPS_ERROR = 'GET_SHOPS_ERROR';

const initialState = {
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_SHOPS:
      return {
        ...state,
        loading: true,
        error: {},
      };
    case GET_SHOPS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        shops: action.shops,
      };
    case GET_SHOPS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const getAllShops = data => async dispatch => {
  dispatch({ type: GET_SHOPS });
  try {
    const res = await Client.post(GET_ALL_SHOPS_API, data);
    dispatch({ type: GET_SHOPS_SUCCESS, shops: res.data });
  } catch (error) {
    dispatch({ type: GET_SHOPS_ERROR, error });
  }
};

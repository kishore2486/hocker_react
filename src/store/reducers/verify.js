import { VERIFY_API } from '../apis';
import Client from '../../axios';

const VERIFY = 'VERIFY';
const VERIFY_PROGRESS = 'VERIFY_PROGRESS';
const VERIFY_ERROR = 'VERIFY_ERROR';

const initialState = {
  loading: false,
  loaded: false,
  isVerified: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case VERIFY:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case VERIFY_PROGRESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        isVerified: true,
      };
    case VERIFY_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        isVerified: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const userVerify = data => async dispatch => {
  dispatch({ type: VERIFY });
  try {
    await Client.post(VERIFY_API, data);
    dispatch({ type: VERIFY_PROGRESS });
  } catch (error) {
    dispatch({ type: VERIFY_ERROR, error });
  }
};

import { SHOP_DETAILS_API } from '../apis';
import Client from '../../axios';

const SHOP_DETAILS = 'SHOP_DETAILS';
const SHOP_DETAILS_SUCCESS = 'SHOP_DETAILS_SUCCESS';
const SHOP_DETAILS_ERROR = 'SHOP_DETAILS_ERROR';

const initialState = {
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case SHOP_DETAILS:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case SHOP_DETAILS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        shopDetails: action.details,
      };
    case SHOP_DETAILS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const getShopDetails = data => async dispatch => {
  dispatch({ type: SHOP_DETAILS });
  try {
    const res = await Client.get(`${SHOP_DETAILS_API}/${data}`);
    dispatch({ type: SHOP_DETAILS_SUCCESS, details: res.data });
  } catch (error) {
    dispatch({ type: SHOP_DETAILS_ERROR, error });
  }
};

import { UPDATE_PROFILE_API } from '../apis';
import Client from '../../axios';

const UPDATE_PROFILE = 'UPDATE_PROFILE';
const UPDATE_PROFILE_SUCCESS = 'UPDATE_PROFILE_SUCCESS';
const UPDATE_PROFILE_ERROR = 'UPDATE_PROFILE_ERROR';

const initialState = {
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_PROFILE:
      return {
        ...state,
        loading: true,
        error: {},
      };
    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
      };
    case UPDATE_PROFILE_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const updateProfile = data => async dispatch => {
  dispatch({ type: UPDATE_PROFILE });
  try {
    const res = await Client.post(UPDATE_PROFILE_API, data);
    if (res) {
      dispatch({ type: UPDATE_PROFILE_SUCCESS });
      return new Promise(resolve => resolve(true));
    }
  } catch (error) {
    dispatch({ type: UPDATE_PROFILE_ERROR, error });
  }
};

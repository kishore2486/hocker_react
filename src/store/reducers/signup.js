import { SIGNUP_API } from '../apis';
import Client from '../../axios';

const REGISTER = 'CREATE_REGISTER';
const REGISTER_SUCCESS = 'CREATED_REGISTER';
const REGISTER_ERROR = 'REGISTER_ERROR';

const initialState = {
  loading: false,
  loaded: false,
  isRegistered: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case REGISTER:
      return {
        ...state,
        loading: true,
        error: {},
      };
    case REGISTER_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        isRegistered: true,
      };
    case REGISTER_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        isRegistered: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const userRegister = data => async dispatch => {
  dispatch({ type: REGISTER });
  try {
    await Client.post(SIGNUP_API, data);
    dispatch({ type: REGISTER_SUCCESS });
  } catch (error) {
    dispatch({ type: REGISTER_ERROR, error });
  }
};

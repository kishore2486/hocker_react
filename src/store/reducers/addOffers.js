import { ADD_OFFERS_API } from '../apis';
import Client from '../../axios';

const ADD_OFFERS = 'ADD_OFFERS';
const ADD_OFFERS_SUCCESS = 'ADD_OFFERS_REGISTER';
const ADD_OFFERS_ERROR = 'ADD_OFFERS_ERROR';

const initialState = {
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_OFFERS:
      return {
        ...state,
        loading: true,
        error: {},
      };
    case ADD_OFFERS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
      };
    case ADD_OFFERS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const addOffers = data => async dispatch => {
  dispatch({ type: ADD_OFFERS });
  try {
    await Client.post(ADD_OFFERS_API, data);
    dispatch({ type: ADD_OFFERS_SUCCESS });
  } catch (error) {
    dispatch({ type: ADD_OFFERS_ERROR, error });
  }
};

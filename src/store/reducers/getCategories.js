import { GET_CATEGORIES_API } from '../apis';
import Client from '../../axios';

const CATEGORIES = 'GET_CATEGORIES';
const CATEGORIES_SUCCESS = 'GOT_CATEGORIES';
const CATEGORIES_ERROR = 'CATEGORIES_ERROR';

const initialState = {
  loading: false,
  loaded: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CATEGORIES:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case CATEGORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        categories: action.categories,
      };
    case CATEGORIES_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const getCategories = () => async dispatch => {
  dispatch({ type: CATEGORIES });
  try {
    const res = await Client.get(GET_CATEGORIES_API);
    dispatch({ type: CATEGORIES_SUCCESS, categories: res.data });
  } catch (error) {
    dispatch({ type: CATEGORIES_ERROR, error });
  }
};

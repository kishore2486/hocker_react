import Cookies from 'js-cookie';
import { LOGIN_API } from '../apis';
import Client from '../../axios';

const LOGIN = 'LOGIN';
const LOGIN_SUCCESS = 'LOGIN_REGISTER';
const LOGIN_ERROR = 'LOGIN_ERROR';

const initialState = {
  loading: false,
  loaded: false,
  isLoggedIn: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        isLoggedIn: true,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        isLoggedIn: false,
        error: action.error,
      };
    case 'CLEAR_LOGIN':
      return { ...initialState };
    default:
      return state;
  }
}

export const userLogin = data => async dispatch => {
  dispatch({ type: LOGIN });
  try {
    const res = await Client.post(LOGIN_API, data);
    dispatch({ type: 'SAVE_USER_DATA', user: res.data });
    if (res) dispatch({ type: LOGIN_SUCCESS });
  } catch (error) {
    dispatch({ type: LOGIN_ERROR, error });
  }
};

export const userLogout = () => dispatch => {
  dispatch({ type: 'LOGOUT' });
  dispatch({ type: 'CLEAR_LOGIN' });
  Cookies.remove('user');
};

export const user = (state = {}, action) => {
  switch (action.type) {
    case 'SAVE_USER_DATA':
      Cookies.set('user', JSON.stringify(action.user), { expires: 5 + 1.67 / 48 });
      return action.user;
    case 'LOGOUT':
      return {};
    default:
      return state;
  }
};

export const userAction = data => dispatch => dispatch({ type: 'SAVE_USER_DATA', user: data });

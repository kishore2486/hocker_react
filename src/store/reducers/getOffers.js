import { GET_OFFERS_API } from '../apis';
import Client from '../../axios';

const GET_OFFERS = 'GET_OFFERS';
const GET_OFFERS_SUCCESS = 'GET_OFFERS_SUCCESS';
const GET_OFFERS_ERROR = 'GET_OFFERS_ERROR';

const initialState = {
  loading: false,
  loaded: false,
  data: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case GET_OFFERS:
      return {
        ...state,
        loading: true,
        error: {},
      };
    case GET_OFFERS_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.data,
      };
    case GET_OFFERS_ERROR:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export const getOffers = () => async dispatch => {
  dispatch({ type: GET_OFFERS });
  try {
    const res = await Client.get(GET_OFFERS_API);
    dispatch({ type: GET_OFFERS_SUCCESS, data: res.data.data });
  } catch (error) {
    dispatch({ type: GET_OFFERS_ERROR, error });
  }
};

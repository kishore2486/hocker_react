import axios from 'axios';
import Cookies from 'js-cookie';

const axiosInstance = axios.create({ baseURL: 'http://localhost:2019' });

axiosInstance.interceptors.request.use(
  conf => {
    const userinfo = Cookies.get('user');
    let token = null;
    if (userinfo) {
      // eslint-disable-next-line prefer-destructuring
      token = JSON.parse(userinfo).token;
      conf.headers.Authorization = `Bearer ${token}`;
    }
    return conf;
  },
  error => Promise.reject(error)
);

axiosInstance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    return Promise.reject(error.response ? error.response.data : String(error));
  }
);

export default axiosInstance;

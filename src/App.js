/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { createBrowserHistory } from 'history';
import { BrowserRouter, Route, Router, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Header from './components/Header';
import Home from './components/Home';
import Footer from './components/Footer';
import Restaurant from './components/UsableDashboard';
import Business from './components/Business';
import ShopDetails from './components/Business/ShopDetails';
import Verify from './components/Verify';
import './App.css';

const history = createBrowserHistory();

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Router history={history}>
          <Route path="/" component={Header} />
          <Route exact path="/" component={Home} />
          <Route exact path="/user/dashboard" component={Restaurant} />
          <Route exact path="/business/:name" component={Business} />
          <Route exact path="/business/:name/:id" component={ShopDetails} />
          <Route exact path="/user/verify/:token" component={Verify} />
          <Route path="/" component={Footer} />
        </Router>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = ({ user }) => ({
  ...user,
});

export default connect(
  mapStateToProps,
  null
)(App);
